官方链接: https://github.com/kubernetes/ingress-nginx/tree/nginx-0.22.0    // 选择tag为0.22.0版本

```shell
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.22.0/deploy/configmap.yaml  //创建tcp/udp服务的confmap
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.22.0/deploy/mandatory.yaml    // 总文件
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.22.0/deploy/namespace.yaml    //创建命名空间
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.22.0/deploy/rbac.yaml   //创建role
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.22.0/deploy/with-rbac.yaml 创建ingress服务

configmap.yaml+ mandatory.yaml+namespace.yaml+rbac.yaml+with-rbac.yaml = mandatory.yaml   // 可以分批创建一个一次性创建

 kubectl get po --all-namespaces // 查看到ingress-nginx命名空间的nginx-ingress-controller-5bbcd59d48-cfl7m 正在运行
```


创建完成之后我们发现nginx的配置文件动态改变了:
```
kubectl exec -it nginx-ingress-controller-5bbcd59d48-cfl7m -n ingress-nginx bash
```

NodePort的缺点
```
采用 NodePort 方式暴露服务面临一个坑爹的问题是，服务一旦多起来,NodePort在每个节点上开启的端口会及其庞大,而且难以维护；
这时候引出的思考问题是 “能不能使用 Nginx 啥的只监听一个端口,比如80,然后按照域名向后转发?"
这思路很好，简单的实现就是使用 DaemonSet 在每个node上监听80，然后写好规则，因为 Nginx外面绑定了宿主机80端口(就像 NodePort)，本身又在集群内，
那么向后直接转发到相应 Service IP 就行了
```

官方的配置文件使用 Deployment 的方式部署单副本到任意一台 worker 机器

官方文档部署完后仍然需要使用Service做转发，在没有slb的情况下仍需使用 NodePort 方式暴露在高端口上

#### Tolerations
默认情况下Kubernetes不在master上部署各种服务 Tolerations 以允许某些 pod 被部署在当前节点，但阻止其它pod的部署
