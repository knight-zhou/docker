#### 安装方法
参考文档： https://jimmysong.io/kubernetes-handbook/practice/traefik-ingress-installation.html

```
拷贝config文件夹下 依次apply 
ingress-rbac.yaml ingress.yaml  traefik.yaml ui.yaml

然后打endpoint:

kubectl label nodes k8s-node1 edgenode=true
kubectl label nodes k8s-node2 edgenode=true

```

创建po的配置文件如下：
```
---
kind: Service
apiVersion: v1
metadata:
  labels:
    app: k-cn
  name: k-cn
  namespace: stg
spec:
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: k-cn
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: k.cn-ingress
  namespace: stg
spec:
  rules:
  - host: k.cn
    http:
      paths:
      - path: /
        backend:
          serviceName: k-cn
          servicePort: 80
```

访问: 把k.cn做host解析到启动了80端口的机器即可访问