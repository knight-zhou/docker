#### 安装
关闭selinux，关闭swap，关闭防火墙
```shell
cd /etc/yum.repos.d/ && rm -rf *
curl -o CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```
master-主节点操作如下：

配置kubernetes的yum源:
```shell
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
        http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```
安装国内docker-ce源
```shell
sudo yum-config-manager \
    --add-repo \
    https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

下一步:
```shell
yum clean all
yum repolist
yum list docker-ce --showduplicates | sort -r   ## 查看版本
yum install docker-ce
yum install  kubeadm
kubeadm config images list    #结果如下
    k8s.gcr.io/kube-apiserver:v1.15.1
    k8s.gcr.io/kube-controller-manager:v1.15.1
    k8s.gcr.io/kube-scheduler:v1.15.1
    k8s.gcr.io/kube-proxy:v1.15.1
    k8s.gcr.io/pause:3.1
    k8s.gcr.io/etcd:3.3.10
    k8s.gcr.io/coredns:1.3.1

##手工拉取以后再取打tag，注意版本号
docker pull mirrorgooglecontainers/kube-apiserver:v1.15.1
docker tag mirrorgooglecontainers/kube-apiserver:v1.15.1 k8s.gcr.io/kube-apiserver:v1.15.1  #打tag

docker pull coredns/coredns:1.3.1
docker tag coredns/coredns:1.3.1 k8s.gcr.io/coredns:1.3.1
        。。。。
		
echo "1" >/proc/sys/net/bridge/bridge-nf-call-iptables

kubeadm init --kubernetes-version=v1.15.1 --pod-network-cidr=10.244.0.0/16 --service-cidr=10.96.0.0/12
然后按提示操作创建隐藏文件夹

### 最后安装网络组建
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

kubectl get pods --all-namespaces -o wide  ##  查看是否为ready状态，到此主节点初始化完毕

```
成功之后：
```
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.5.192.65:6443 --token hcv3sp.yesicznlwzu6fid2 \
    --discovery-token-ca-cert-hash sha256:d305c2f2e86e2d70399360678d8e6be314bfd1e6e532d0a2ea98f81d00e049e5
```

从节点配置:
```shell
cd /etc/yum.repo.d/
cp 配置文件
yum install -y kubelet kubeadm kubectl
```

加入从节点：

```
[root@k8s-node2 ~]# kubeadm join 192.168.0.80:6443 --token czikfp.8huw8sy58x3eyv36 \
>     --discovery-token-ca-cert-hash sha256:22282b69add0e46ce2b12938c56a28a555cd9c4becd9fb8ee10c480b602e9623
[preflight] Running pre-flight checks
        [WARNING Service-Docker]: docker service is not enabled, please run 'systemctl enable docker.service'
        [WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
        [WARNING Service-Kubelet]: kubelet service is not enabled, please run 'systemctl enable kubelet.service'
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.15" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Activating the kubelet service
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

主节点查看:
```
kubectl get pods -n kube-system -o wide
kubectl get nodes
```

从节点再配置:
```
 docker pull mirrorgooglecontainers/pause:3.1
 docker tag mirrorgooglecontainers/pause:3.1 k8s.gcr.io/pause:3.1
 
 docker pull mirrorgooglecontainers/kube-proxy:v1.15.1
 docker tag mirrorgooglecontainers/kube-proxy:v1.15.1 k8s.gcr.io/kube-proxy:v1.15.1
```

开机关闭swap
```shell
echo "swapoff -a" >> /etc/rc.local

systemctl enable kubelet.service   # 开机启动

echo "vm.swappiness = 0">> /etc/sysctl.conf  # 禁用虚拟内存

```
注意:
```
yum install docker-ce-18.09.8-3.el7   // 安装指定版本的docker
```