## 注意
镜像就相当于iso文件.容器就相当于虚拟机.在导出镜像的时候 导出的是容器的id 也就是导出的虚拟机.

## start docker
```
/bin/systemctl start docker.service or systemctl start docker

service docker start

docker images    # 查找本地镜像

docker search lamp  #查找我们需要的镜像

docker pull ubuntu    # 获取docker镜像 docker pull name[:tag] tag标签是版本号

# -t选项让docker分配一个伪终端并且绑定到容器的标准输入上，-i 让标准输入保持打开
docker run -ti docker.io/ubuntu /bin/bash     

#删除镜像
docker rmi -f docker.io/ubuntu

# 创建镜像
docker run -ti docker.io/ubuntu /bin/bash   首先进入终端记住id
exit
docker commit -m "mkdir zlong dirctory" -a "zlong" 5f73768499bd knight
docker images  查看镜像

#然后进入我们刚才创建的镜像
docker run -ti knight /bin/bash

```
## Docker创建容器
```
#创建容器
docker create -it docker.io/ubuntu:latest
docker create -it knight

#查看容器进程
docker ps -a

# 查看正在运行的容器
docker ps

# 启动容器
docker start 136f66c4416e

docker ps -a      #看STATUS

#停止和重启容器 
docker start 136f66c4416e
docker restart 136f66c4416e

# 守护进程状态运行
docker run -d knight /bin/bash -c "while true;do echo knight; sleep 1;done"


#进入容器
docker exec -ti 340b84d0f791 /bin/bash

# 删除容器
docker rm 340b84d0f791

```

## 更深入一点
```
# 导出镜像(可以导出正在运行的也可以导出已经终止的)
docker export aa9aa5ecbc60 > knight_img.tar

#导入镜像
cat knight_img.tar |docker import - version01

#查看刚才导入的镜像
docker images

```
### 创建本地仓库
```
# 启动一个私有仓库服务监听端口是5000,默认会在将仓库放在/tmp/registry目录下 也可以通过-v指定目录
docker run -d -p 5000:5000 registry

#可以看到坚挺的端口5000
docker ps

96366790c3f4 registry "docker-registry" About a minute ago Up About a minute 0.0.0.0:5000->5000/tcp stoic_noyce

```

### Docker管理本地仓库
```
docker tag 镜像标签 ip端口/自定义的name
docker tag knight:latest 127.0.0.1:5000/cangku_ceshi
```

##上传和下载私有仓库
```
docker push 127.0.0.1:5000/cangku_ceshi  上传,本机和别的电脑都可以上传和下载

docker pull 127.0.0.1:5000/cangku_ceshi    下载,本机和别的电脑都可以上传和下载

```