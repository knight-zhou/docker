[学习资源](http://udn.yyuap.com/doc/docker_practice/advanced_network/port_mapping.html)


## 镜像安装
```
centos6.x安装：
#wget http://mirrors.ustc.edu.cn/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
#wget   http://mirrors.ustc.edu.cn/fedora/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
#docker 下ubutu镜像包：
docker  pull   ubuntu   #下载ubuntu所有的镜像
docker  pull   ubuntu:14.10    下载ubuntu14.10镜像

#docker 下载centos镜像
docker  pull   centos   #下载centos所有的镜像
docker  pull   centos:centos6    下载centos6镜像

#查看本机所有的镜像包：
docker  images
进入centos6的镜像：
docker run -i -t centos:centos6 /bin/bash
进入centos6的镜像：
docker run -i -t centos /bin/bash
```
我们使用centos6的镜像:

![](img/1.png)


docker ps -a -q列出当前运行的容器， -a 会列出所有，包括已停止的， -q 只列出容器 ID。 docker ps -a -q | xargs docker rm 可以删除所有的容器。
下面使用docker ps –a列出所有的容器，包括已经Exit。
注意：一个镜像可以使用多个容器，每个容器的主机名不同也就是CONTAINER ID不同。默认情况下 CONTAINER ID和容器的hostname那么一致 。

## docker如何进入容器？
当时使用交互docker run -i -t centos:centos6 /bin/bash 进入images的时候，每次进入都是新的容器。如何以前修改的过的容器了？

![](./img/2.png)

进入容器方法：
docker exec -it ea756825c756 /bin/bash
退出容器的正确步骤是关闭终端，而不是使用exit。

如果提示 Error response from daemon: Container 2173d840c78b is not running
这样就需要先启动容器：（容器中的软件要让外网访问，需要把端口映射到本机的端口上才可以）
方法如下：
docker start  Container id.

到此已经可以运行docker容器中的程序了。现在要做的就是把容器的应用发不出来也就是把容器额应用映射到本地从而发布。
具体方法下面的文章

### 举例：
使用 hostPort:containerPort 格式本地的 5000 端口映射到容器的 5000 端口，可以执行
```
$ sudo docker run -d -p 5000:5000 training/webapp python app.py
```
也可以采用桥接方式，自己配置ip，这样就不是使用nat，这样不安全。不推荐使用，具体方法可以bing
从上面的例子可以看出，docker还是不如私有云cirix xen.
从管理角度上不如私有云，从资源利用率来讲确实提高了不少。





