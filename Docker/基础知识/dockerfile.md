#### 编写
```
FROM centos:6
MAINTAINER knight
```
#### create images,使用点 默认会去当前路径去find
```
docker build -t test:v2 .
```
####  语法规则
### FROM 指定基础镜像

### RUN 执行shell命令
```
FROM centos:6
RUN yum install lrzsz && mkdir /opt/tools
```
#### COPY
你COPY的文件必须跟dockerfile在同一个文件夹下,否则就会报错.所以我们要用到COPY的时候,最好组织一下目录结构


```
COPY package.json /usr/src/app/
COPY web/m-cn /home/data/webroot/m-cn  // 默认只能拷贝文件 拷贝文件夹需要在后面加上
```

#### rm 是退出就删除容器
```
docker run -it --rm centos:6 cat /etc/passwd
```
####  CWD ,启动容器run 后面接的东西，默认是/bin/bash。意思是启动容器去执行命令
```
CMD [ "curl", "-s", "http://ip.cn" ]

```

#### ENTRYPOINT (docker run 接的参数)
```
FROM centos:6
...
RUN addgroup -S redis && adduser -S -G redis redis
...
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 6379
CMD [ "redis-server" ]

```
docker-entrypoint.sh
```
#!/bin/sh
...
# allow the container to be started with `--user`
if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
    chown -R redis .
    exec su-exec redis "$0" "$@"
fi

exec "$@"
```

#### 挂载
VOLUME /data

运行时可以覆盖挂载设置
```
docker run -d -v /home/data:/data xxxx     
```

##### 健康检查(假设我们有个镜像是个最简单的 Web 服务，我们希望增加健康检查来判断其 Web 服务是否在正常工作，我们可以用 curl 来帮助判断)
```
FROM nginx
RUN apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*
HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -fs http://localhost/ || exit 1
```
#### WORKDIR 功能,以下两种是等价相同的
```
RUN cd /app ; echo fff > la.txt      

等价于:

WORKDIR /app
RUN echo fff > la.txt
 
```

#### docker save 和 docker load
保存镜像
```
$ docker save alpine | gzip > alpine-latest.tar.gz
```
然后我们将 alpine-latest.tar.gz 文件复制到了到了另一个机器上，可以用下面这个命令加载镜像：
```
$ docker load -i alpine-latest.tar.gz
Loaded image: alpine:latest
```
一句命令搞定
```
docker save <镜像名> | bzip2 | pv | ssh <用户名>@<主机名> 'cat | docker load'
```

