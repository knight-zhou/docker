## 数据卷类似nas
```
docker volume create my-vol
docker volume ls
docker volume inspect my-vol
```

## 启动一个挂载数据卷的容器
```
docker run -d -P \
    --name web \
    # -v my-vol:/wepapp \
    --mount source=my-vol,target=/webapp \
	/bin/bash -D
    
```