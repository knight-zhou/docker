#### 端口映射
```
启动容器 并且映射端口:
```
docker run --name baota -it -p 10022:22 centos:6 /bin/bash -D

docker run -tid --name c7 centos:7 /usr/sbin/init

docker run --name baota -it -p 10022:22 -p 81:80 centos:6 /bin/bash -D

ctrl+q   #退出
```

再次进入:
docker exec -it cid /bin/bash

重命名容器:
docker rename base_centos6 vm1_centos6

```


#### 启动容器
```
docker run -h=”salt_zabbix_manager02″  –name salt_zabbix_manager02 -d -p 52000:22 -p   52001:3306  ubuntu /etc/rc.local

上述启动参数解释：
-h  是指启动后容器中的主机名。
–name 是宿主机上容器的名称，以后启动停止容器不必用容器ID，用名称即可，如docker stop redis-test。
-d   以后台形式运行。
-p   指定映射端口，如果需要映射UDP端口，则格式是 -p3000:3000/udp。
debian02  是基础镜像名称。

/etc/rc.local  是容器的启动命令，把多个启动脚本放/etc/rc.local中，方便多个程序随容器开机自启动。
```

#### 打成新的镜像,方便以后还原镜像
```
docker commit baota knight_centos6_v1

baota(运行容器名称)   knight_centos6_v1(生成镜像名称)
运行镜像并添加端口
docker run -d -it -p 8000:80 tang1:latest /bin/bash
```

#### 启动和停止镜像
docker start cid

docker stop cid

#### 删除容器
docker rm cid

#### 打包镜像
docker commit cid loudong_v1:v1

`docker commit` 命令除了学习之外，还有一些特殊的应用场合，比如被入侵后保存现场等。但是，不要使用 docker commit 定制镜像，定制镜像应该使用 Dockerfile 来完成

#### del image
docker image rm centos

#### list detail infomation
docker image ls --digests

#### 重命名镜像打tag
```
[root@hk-knight ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
knight_iso_all      latest              03f5c7010743        10 minutes ago      393 MB
docker.io/centos    6                   70b5d81549ec        12 days ago         195 MB
[root@hk-knight ~]# docker tag 03f5c7010743 knight_centos6_iso_all
[root@hk-knight ~]# docker images
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
knight_centos6_iso_all   latest              03f5c7010743        10 minutes ago      393 MB
knight_iso_all           latest              03f5c7010743        10 minutes ago      393 MB
docker.io/centos         6                   70b5d81549ec        12 days ago         195 MB
[root@hk-knight ~]# docker rmi knight_iso_all
Untagged: knight_iso_all:latest
[root@hk-knight ~]# docker rmi 03f5c7010743
Untagged: knight_centos6_iso_all:latest
Deleted: sha256:03f5c70107439ff77fb19920ebeb63934b4f6bfc527d8a5454209a5a0ccb8afd
Deleted: sha256:c0cf1ef2dc6f91944610afcb02cc6782bf1c2eb2271c3dbad1f20881f9775a0f
[root@hk-knight ~]# docker images            
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
docker.io/centos    6                   70b5d81549ec        12 days ago         195 MB
```

#### 常用命令
```
(1)出已经下载下来的镜像，
docker image ls

(2)check 镜像体积
docker system df

```

