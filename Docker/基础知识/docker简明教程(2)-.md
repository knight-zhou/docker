## Docker挂在一个主机目录到容器中作为数据卷
```
docker run -d -v /src/:/src/ docker.io/ubuntu:latest /bin/sh -c "while true;do echo fwc; sleep 1;done"

docker exec -ti 65c182206e68 /bin/bash

# 查看容器中的src目录的内容看看它是不是挂载本机,发现已经挂载
ls /src/   

```

## Docker数据卷容器
Docker数据卷管理器就是一个一般的容器专门供其他容器挂载,

首先创建一个名字是dbdata的数据卷容器创建一个数据卷载到 /dbdata
```
docker run -ti -v /dbdata --name dbdata docker.io/ubuntu
cd /dbdata && touch 555 666

```
现在我们在其它俩容器中挂载这个数据卷容器,创建一个容器挂载刚才创建的数据卷容器
```
docker run -ti --volumes-from dbdata --name db_knight1 docker.io/ubuntu

ls /dbdata/  发现挂载成功

我们试图修改数据卷的内容，是立马生效的对所有挂载的都生效 这个和nfs挂载 一个样
```
## Docker删除容器中的数据卷
```
数据卷容器删除
docker rm 949c4ad585
docker rm 34509b0
docker rm -v d1cc974  删除最后一个的时候 加上-v参数就行了
```
## Docker容器内端口映射到本地端口
```
docker run -d -P training/webapp python app.py    # 随机指定端口进行映射

docker ps -l     #可以看到本地端口跟容器内端口相映射我们可以通过访问32770访问容器内端口号是5000的应用

docker attach 51a7c5d87714   # 通过attach 进入容器

docker exec -it 51a7c5d87714 /bin/bash   # 通过exec进入docker容器

#指定的端口进行映射,-p是小写的
docker run -d -p 192.168.31.68:5300:5000 training/webapp python app.py
docker run -d -p 0.0.0.0:5300:80 training/webapp python app.py

#查看端口映射情况
docker ps -l

#利用命令查看容器的详细信息
docker inspect 44ab452b4

# 查看容器的CPU利用率、内存的使用量以及可用内存总量
docker stats 51a7c5d87714

```






