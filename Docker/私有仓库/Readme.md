环境介绍:

192.168.106.162-me03-持续集成-镜像仓库 
```
1、获取镜像
shell>yum update 跟新源
shell>docker pull registry:2 下载registry:2镜像 
shell>docker image ls 查看下载的镜像

2、运行镜像
shell>docker run -d -p 5000:5000 -v /myregistry:/var/lib/registry registry:2
-d 后台运行
-p 将容器的短裤哦5000映射到Host的5000端口，5000端口是registry的服务端口
-v 将容器的/var/lib/registry目录映射到Host的/myregistry,用于存放镜像数据
--name 为运行的容器命名

shell> docker ps 查看容器是否运行成功
到此私有仓库服务端搭建完成

```

192.168.106.161-me02-work节点:

一下全是work节点操作
```
cat /usr/lib/systemd/system/docker.service
添加
ExecStart=/usr/bin/dockerd  --insecure-registry 192.168.106.162:5000

```
然后重启: 

systemctl daemon-reload
systemctl restart docker
```
docker tag busybox:v1 192.168.106.162:5000/busybox
docker push 192.168.106.162:5000/busybox

repository 的完整格式为：[registry-host]:[port]/[username]/xxx
Docker Hub 上的镜像可以省略 [registry-host]:[port]
```






