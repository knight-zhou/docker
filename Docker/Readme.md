##### 安装
[docker-ce官方文档](https://docs.docker.com/install/linux/docker-ce/centos/)

#### docker常用命令
```go
docker build -t knight:v1 .  // 打镜像
docker inspect xx   //可以查看详细信息 
docker exex -it xxx /bin/bash  // 执行shell 
docker run  -i -t knight:v1 /bin/bash   // 运行指定镜像并进入bash

```

